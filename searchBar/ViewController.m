//
//  ViewController.m
//  searchBar
//
//  Created by Click Labs133 on 10/6/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *countryNames;
NSMutableArray *showCountries;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableData;
@property (strong, nonatomic) IBOutlet UIButton *deleteBtn;

@end

@implementation ViewController
@synthesize searchBar;
@synthesize tableData;
@synthesize deleteBtn;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    countryNames=[NSMutableArray arrayWithObjects:@"Afghanistan"
                 ,@"Albania"
                 ,@"Algeria"
                 ,@"Andorra"
                 ,@"Angola"
                 ,@"Antigua and Barbuda"
                 ,@"Argentina"
                 ,@"Armenia"
                 ,@"Aruba"
                 ,@"Australia"
                 ,@"Austria"
                 ,@"Azerbaijan"
                 ,@"Bahamas, The"
                 ,@"Bahrain"
                 ,@"Bangladesh"
                 ,@"Barbados"
                 ,@"Belarus"
                 ,@"Belgium"
                 ,@"Belize"
                 ,@"Benin"
                 ,@"Bhutan"
                 ,@"Bolivia"
                 ,@"Bosnia and Herzegovina"
                 ,@"Botswana"
                 ,@"Brazil"
                 ,@"Brunei "
                 ,@"Bulgaria"
                 ,@"Burkina Faso"
                 ,@"Burma"
                 ,@"Burund"
                 ,@"Cambodia"
                 ,@"Cameroon"
                 ,@"Canada"
                 ,@"Cape Verde"
                 ,@"Central African Republic"
                 ,@"Chad"
                 ,@"Chile"
                 ,@"China"
                 ,@"Colombia"
                 ,@"Comoros"
                 ,@"Congo,Democratic Republic of the"
                 ,@"Congo,Republic of the"
                 ,@"CostaRica"
                 ,@"Cote d'Ivoire"
                 ,@"Croatia"
                 ,@"Cuba"
                 ,@"Curacao"
                 ,@"Cyprus"
                 ,@"Czech Republic"
                 ,@"Denmark"
                 ,@"Djibouti"
                 ,@"Dominica"
                 ,@"Dominican Republic"
                 ,@"Ecuador"
                 ,@"Egypt"
                 ,@"El Salvador"
                 ,@"Equatorial Guinea"
                 ,@"Eritrea"
                 ,@"Estonia"
                 ,@" Ethiopia"
                 ,@"Fiji"
                 ,@"Finland"
                 ,@"France"
                 ,@"Gabon"
                 ,@"Gambia, The"
                 ,@"Georgia"
                 ,@"Germany"
                 ,@"Ghana"
                 ,@"Greece"
                 ,@"Grenada"
                 ,@"Guatemala"
                 ,@"Guinea"
                 ,@"Guinea-Bissau"
                 ,@"Guyana"
                 ,@"Haiti"
                 ,@"Holy See"
                 ,@"Honduras"
                 ,@"Hong Kong"
                 ,@"Hungary"
                 ,@"Iceland"
                 ,@"India"
                 ,@"Indonesia"
                 ,@"Iran"
                 ,@"Iraq"
                 ,@"Ireland"
                 ,@"Israel"
                 ,@"Italy"
                 ,@"Jamaica"
                 ,@"Japan"
                 ,@"Jordan"
                 ,@"Kazakhstan"
                 ,@"Kenya"
                 ,@"Kiribati"
                 ,@"Korea, North"
                 ,@"Korea, South"
                 ,@"Kosovo"
                 ,@"Kuwait"
                 ,@"Kyrgyzstan"
                 ,@"Laos"
                 ,@"Latvia"
                 ,@"Lebanon"
                 ,@"Lesotho"
                 ,@"Liberia"
                 ,@"Libya"
                 ,@"Liechtenstein"
                 ,@"Lithuania"
                 ,@"Luxembourg"
                 ,@"Macau"
                 ,@"Macedonia"
                 ,@"Madagascar"
                 ,@"Malawi"
                 ,@" Malaysia"
                 ,@"Maldives"
                 ,@"Mali"
                 ,@"Malta"
                 ,@"Marshall Islands"
                 ,@"Mauritania"
                 ,@"Mauritius"
                 ,@"Mexico"
                 ,@"Micronesia"
                 ,@"Moldova"
                 ,@"Monaco"
                 ,@"Mongolia"
                 ,@"Montenegro"
                 ,@"Morocco"
                 ,@"Mozambique"
                 ,@"Namibia"
                 ,@"Nauru"
                 ,@"Nepal"
                 ,@"Netherlands"
                 ,@"Netherlands Antilles"
                 ,@"New Zealand"
                 ,@"Nicaragua"
                 ,@"Niger"
                 ,@"Nigeria"
                 ,@"North Korea"
                 ,@"Norway"
                 ,@"Oman"
                 ,@"Pakistan"
                 ,@"Palau"
                 ,@"Palestinian Territories"
                 ,@"Panama"
                 ,@"Papua New Guinea"
                 ,@"Paraguay"
                 ,@"Peru"
                 ,@"Philippines"
                 ,@"Poland"
                 ,@"Portugal"
                 ,@"Qatar"
                 ,@"Romania"
                 ,@"Russia"
                 ,@"Rwanda"
                 ,@"Saint Kitts and Nevis"
                 ,@"Saint Lucia"
                 ,@"Saint Vincent and the Grenadines"
                 ,@"Samoa"
                 ,@"San Marino"
                 ,@"Sao Tome and Principe"
                 ,@"Saudi Arabia"
                 ,@"Senegal"
                 ,@"Serbia"
                 ,@"Seychelles"
                 ,@"Sierra Leone"
                 ,@"Singapore"
                 ,@"Sint Maarten"
                 ,@"Slovakia"
                 ,@"Slovenia"
                 ,@"Solomon Islands"
                 ,@"Somalia"
                 ,@"South Africa"
                 ,@"South Korea"
                 ,@"South Sudan"
                 ,@"Spain "
                 ,@"Sri Lanka"
                 ,@"Sudan"
                 ,@"Suriname"
                 ,@"Swaziland "
                 ,@"Sweden"
                 ,@"Switzerland"
                 ,@"Syria"
                 ,@"Taiwan"
                 ,@"Tajikistan"
                 ,@"Tanzania"
                 ,@"Thailand"
                 ,@"Timor-Leste"
                 ,@"Togo"
                 ,@"Tonga"
                 ,@"Trinidad and Tobago"
                 ,@"Tunisia"
                 ,@"Turkey"
                 ,@"Turkmenistan"
                 ,@"Tuvalu"
                 ,@"Uganda"
                 ,@"Ukraine"
                 ,@"United Arab Emirates"
                 ,@"United Kingdom"
                 ,@"Uruguay"
                 ,@"Uzbekistan"
                 ,@"Vanuatu"
                 ,@"Venezuela"
                 ,@"Vietnam"
                 ,@"Yemen"
                 ,@"Zambia"
                 ,@"Zimbabwe",nil];
    
    
    
   showCountries=[NSMutableArray arrayWithArray:countryNames];
    [tableData reloadData];
    [deleteBtn addTarget:self action:@selector(deleteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
}
- (IBAction)deleteBtnPressed:(id)sender {
 [tableData setEditing:YES animated:YES];
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if(editingStyle==UITableViewCellEditingStyleDelete)
        
    {
        
        [showCountries removeObjectAtIndex:indexPath.row];
        
        [tableData deleteRowsAtIndexPaths:[NSMutableArray arrayWithObjects:indexPath, nil ] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return showCountries.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell1"];
    //[self performSegueWithIdentifier:@"goToScreen2" sender:self];
    cell.textLabel.text=showCountries[indexPath.row];
    cell.textColor=[UIColor whiteColor];
    cell.backgroundColor=[UIColor orangeColor];
    return cell;

}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText

{
    
    NSPredicate *resultPredicate=[NSPredicate predicateWithFormat: @"SELF contains[c] %@",searchText];
    
    showCountries=[NSMutableArray arrayWithArray:[countryNames filteredArrayUsingPredicate:resultPredicate]];
    
    [tableData reloadData];
    
    //listOfNames.hidden=YES;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
